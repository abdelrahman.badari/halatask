//
//  HalaaTaskUITests.swift
//  HalaaTaskUITests
//
//  Created by Abdelrahman Badary on 01/03/2024.
//

import XCTest

final class HalaaTaskUITests: XCTestCase {

    var app: XCUIApplication!

    override func setUpWithError() throws {
        continueAfterFailure = false
        app = XCUIApplication()
        app.launchEnvironment["UITesting"] = "true"
        app.launch()
    }

    func testLargeWidgetDisplay() {
        let largeWidget = app.otherElements.matching(identifier: "LargeWidget")

        XCTAssertEqual(largeWidget.count, 1, "The large widget should be displayed on the screen.")
     }
    
    func testMiniWidgetsDisplay() {
         let miniWidgets = app.otherElements.matching(identifier: "MiniWidget")
         XCTAssertEqual(miniWidgets.count, 4, "There should be four mini widgets displayed.")
     }

     func testSquareWidgetsDisplay() {
         let squareWidgets = app.otherElements.matching(identifier: "SquareWidget")
         XCTAssertEqual(squareWidgets.count, 1, "There should be two square widgets displayed.")
     }  
}
