//
//  HalaaTaskApp.swift
//  HalaaTask
//
//  Created by Abdelrahman Badary on 01/03/2024.
//

import SwiftUI

@main
struct HalaaTaskApp: App {
    var body: some Scene {
        WindowGroup {
            if ProcessInfo.processInfo.environment["UITesting"] == "true" {
                let viewModel = WidgetsDashboardViewModel(widgetsRepo: WidgetsTestingRepo())
                WidgetDashboardView(viewModel: viewModel)
            } else {
                let viewModel = WidgetsDashboardViewModel(widgetsRepo: WidgetsLocalRepo())
                WidgetDashboardView(viewModel: viewModel)
            }
        }
    }
}
