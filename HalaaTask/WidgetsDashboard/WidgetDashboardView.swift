//
//  WidgetDashboardView.swift
//  HalaaTask
//
//  Created by Abdelrahman Badary on 01/03/2024.
//

import SwiftUI

struct WidgetDashboardView: View {
    @ObservedObject var viewModel: WidgetsDashboardViewModel
    let screenWidth = UIScreen.main.bounds.width
    let stackSpacing: CGFloat = 8 // Space between widgets horizontally and vertically
    let edgePadding: CGFloat = 20

    var body: some View {
        ScrollView {
            VStack(spacing: stackSpacing) { // This adds vertical space between each row
                ForEach(viewModel.getWidgetRows(), id: \.self) { rowWidgets in
                    if rowWidgets.first?.style == "large" {
                        // Large widgets take the full width
                        LargeWidget()
                            .frame(width: screenWidth - (edgePadding * 2), height: 150)
                            .padding(.horizontal, edgePadding)
                    } else {
                        HStack(spacing: stackSpacing) { // This adds horizontal space between each widget
                            ForEach(rowWidgets) { widget in
                                if widget.style == "square" {
                                    SquareWidget()
                                        .frame(width: (screenWidth / 2) - edgePadding - (stackSpacing / 2), height: (screenWidth / 2) - edgePadding - (stackSpacing / 2))
                                } else if widget.style == "mini" {
                                    // Minis are handled below
                                    EmptyView()
                                }
                            }
                            // Align mini widgets in two columns, matching the height of the square widget
                            if rowWidgets.contains(where: { $0.style == "square" }) {
                                VStack(spacing: stackSpacing) {
                                    // Calculate total number of mini widgets
                                    let totalMiniWidgets = rowWidgets.filter { $0.style == "mini" }.count
                                    // Calculate number of rows needed based on total mini widgets (2 widgets per row)
                                    let numberOfRows = (totalMiniWidgets + 1) / 2
                                    
                                    ForEach(0..<numberOfRows, id: \.self) { rowIndex in
                                        HStack(spacing: stackSpacing) {
                                            // Calculate the starting index for the mini widgets in the current row
                                            let startIndex = rowIndex * 2
                                            // Calculate the ending index, ensuring it does not exceed the total number of mini widgets
                                            let endIndex = min(startIndex + 2, totalMiniWidgets)
                                            // Display the mini widgets for the current row
                                            ForEach(rowWidgets.filter { $0.style == "mini" }[startIndex..<endIndex], id: \.self) { widget in
                                                MiniWidget()
                                                    .frame(width: (screenWidth / 4) - 10 - (stackSpacing / 2), height: (screenWidth / 4) - 10 - (stackSpacing / 2))
                                            }
                                        }
                                    }
                                }.frame(height: (screenWidth / 2) - edgePadding - (stackSpacing * 2)) // Adjust height to match
                            } else if !rowWidgets.contains(where: { $0.style == "square" }) {
                                // Handle rows of only mini widgets
                                ForEach(rowWidgets.filter { $0.style == "mini" }) { widget in
                                    MiniWidget()
                                        .frame(width: (screenWidth / 4) - 10 - (stackSpacing / 2), height: (screenWidth / 4) - 10 - (stackSpacing / 2))
                                }
                            }
                        }.padding(.horizontal, edgePadding) // Add horizontal padding to the HStack
                    }
                }
            }
        }
    }
}

struct SquareWidget: View {
    var body: some View {
        Rectangle()
            .foregroundColor(.blue)
            .cornerRadius(8.0)
            .accessibilityIdentifier("SquareWidget")

    }
}

struct MiniWidget: View {
    var body: some View {
        Rectangle()
            .foregroundColor(.green)
            .cornerRadius(8.0)
            .accessibilityIdentifier("MiniWidget")

    }
}

struct LargeWidget: View {
    var body: some View {
        Rectangle()
            .foregroundColor(.red)
            .cornerRadius(8.0)
            .accessibilityIdentifier("LargeWidget")

    }
}



struct WidgetDashboardView_Previews: PreviewProvider {
    static var previews: some View {
        WidgetDashboardView(viewModel: WidgetsDashboardViewModel(widgetsRepo: WidgetsLocalRepo()))
    }
}
