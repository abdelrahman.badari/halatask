//
//  WidgetsDashboardViewModel.swift
//  HalaaTask
//
//  Created by Abdelrahman Badary on 01/03/2024.
//

import Foundation

class WidgetsDashboardViewModel: ObservableObject {
    @Published var widgets: [Widget] = []
    private var widgetsRepo: WidgetsRepoProtocol
    init(widgetsRepo: WidgetsRepoProtocol) {
        self.widgetsRepo = widgetsRepo
        self.widgets = widgetsRepo.getWidgets()
      }
    
    private func loadWidgets(from jsonData: Data) {
          do {
              self.widgets = try JSONDecoder().decode([Widget].self, from: jsonData)
          } catch {
              print("Error decoding JSON: \(error)")
          }
      }

    private func loadWidgets() {
        let jsonData = """
        [
            {
                "style": "square"
            },
            {
                "style": "mini"
            },
            {
                "style": "mini"
            },
            {
                "style": "mini"
            },
            {
                "style": "mini"
            },
            {
                "style": "square"
            },
            {
                "style": "square"
            },
            {
                "style": "large"
            },
            {
                "style": "mini"
            },
            {
                "style": "mini"
            },
            {
                "style": "mini"
            },
            {
                "style": "mini"
            },
            {
                "style": "large"
            },

        ]
        """.data(using: .utf8)!
        
        do {
            self.widgets = try JSONDecoder().decode([Widget].self, from: jsonData)
        } catch {
            print("Error decoding JSON: \(error)")
        }
    }

    func getWidgetRows() -> [[Widget]] {
        var rows: [[Widget]] = []
        var currentRow: [Widget] = []
        var miniCount = 0
        var squareCount = 0

        for widget in widgets {
            switch widget.style {
            case "large":
                // Large widgets always start a new row
                if !currentRow.isEmpty {
                    // Finish the current row if it's not empty
                    rows.append(currentRow)
                }
                rows.append([widget]) // Start a new row with the large widget
                // Reset counters
                currentRow = []
                miniCount = 0
                squareCount = 0
            case "square":
                squareCount += 1
            case "mini":
                miniCount += 1
            default:
                break // Ignore unknown widget types
            }

            // Check if the current widget can fit in the current row or if it starts a new row
            if widget.style != "large" {
                currentRow.append(widget)
            }

            // Check if the current row is full
            if squareCount == 2 || miniCount == 4 || (squareCount == 1 && miniCount == 4) {
                rows.append(currentRow)
                // Reset for a new row
                currentRow = []
                miniCount = 0
                squareCount = 0
            }
        }

        // Add the remaining widgets if the last row is not empty
        if !currentRow.isEmpty {
            rows.append(currentRow)
        }

        return rows
    }
}
