//
//  WidgetModel.swift
//  HalaaTask
//
//  Created by Abdelrahman Badary on 01/03/2024.
//

import Foundation

struct Widget: Identifiable, Decodable, Hashable, Equatable {
    let id = UUID()
    var style: String
    
    static func == (lhs: Widget, rhs: Widget) -> Bool {
        return lhs.id == rhs.id && lhs.style == rhs.style
    }
}
