//
//  WidgetsLocalRepo.swift
//  HalaaTask
//
//  Created by Abdelrahman Badary on 04/03/2024.
//

import Foundation
class WidgetsLocalRepo: WidgetsRepoProtocol {
    func getWidgets() -> [Widget] {
        guard let url = Bundle.main.url(forResource: "WidgetsDashboard", withExtension: "json") else {
            print("JSON file not found")
            return []
        }
        
        do {
               let data = try Data(contentsOf: url)
               let decoder = JSONDecoder()
               let jsonData = try decoder.decode([Widget].self, from: data)
               return jsonData
           } catch {
               print("Error reading or decoding JSON: \(error)")
               return []
           }
        
    }
}
