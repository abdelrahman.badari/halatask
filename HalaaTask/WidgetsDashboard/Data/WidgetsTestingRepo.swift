//
//  WidgetsUITestingRepo.swift
//  HalaaTask
//
//  Created by Abdelrahman Badary on 04/03/2024.
//

import Foundation

//This repo's whole purpose is for UI tests to be able to run the app with mocked data, ofcourse this implementation could be avoided if we use a mocking library but it is also good representation of the Repo pattern 
class WidgetsTestingRepo: WidgetsRepoProtocol {
    func getWidgets() -> [Widget] {
        return [Widget(style: "large"),
                Widget(style: "square"),
                Widget(style: "mini"),
                Widget(style: "mini"),
                Widget(style: "mini"),
                Widget(style: "mini")]
    }
}
