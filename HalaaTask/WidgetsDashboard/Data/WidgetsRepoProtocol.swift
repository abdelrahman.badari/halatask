//
//  WidgetsRepo.swift
//  HalaaTask
//
//  Created by Abdelrahman Badary on 04/03/2024.
//

import Foundation
protocol WidgetsRepoProtocol {
    func getWidgets() -> [Widget]
}


