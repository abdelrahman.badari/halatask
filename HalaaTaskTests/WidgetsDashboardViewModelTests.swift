//
//  WidgetsDashboardViewModelTests.swift
//  HalaaTaskTests
//
//  Created by Abdelrahman Badary on 01/03/2024.
//

import XCTest
@testable import HalaaTask

final class WidgetsDashboardViewModelTests: XCTestCase {
    
    var viewModel: WidgetsDashboardViewModel!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
        viewModel = WidgetsDashboardViewModel(widgetsRepo: WidgetsTestingRepo())
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        viewModel = nil
        super.tearDown()
    }

    func testGetWidgetRowsForEmptyWidgets() throws {
        // Test with no widgets
        viewModel.widgets = []
        let rows = viewModel.getWidgetRows()
        XCTAssertTrue(rows.isEmpty, "Rows should be empty when there are no widgets.")
    }

    func testGetWidgetRowsForMixedWidgets() throws {
        // Test with a mix of widgets
        viewModel.widgets = [
            Widget(style: "square"), Widget(style: "mini"), Widget(style: "mini"),
            Widget(style: "large"), Widget(style: "square"), Widget(style: "mini"),
            Widget(style: "mini"), Widget(style: "mini"), Widget(style: "mini"),
            Widget(style: "large")
        ]
        let rows = viewModel.getWidgetRows()
        XCTAssertEqual(rows.count, 4, "There should be 4 rows for the mixed widgets.")
        XCTAssertEqual(rows[0].count, 3, "The first row should contain 3 widgets.")
        XCTAssertEqual(rows[1].count, 1, "The second row (large widget) should contain 1 widget.")
        XCTAssertEqual(rows[2].count, 5, "The third row should contain 4 widgets.")
        XCTAssertEqual(rows[3].count, 1, "The fourth row (large widget) should contain 1 widget.")
    }

    func testGetWidgetRowsForSingleStyleWidgets() throws {
        // Test with only one style of widgets
        viewModel.widgets = Array(repeating: Widget(style: "square"), count: 4)
        let rows = viewModel.getWidgetRows()
        XCTAssertEqual(rows.count, 2, "There should be 2 rows when there are 4 square widgets.")
    }
}
