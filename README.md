
# Hala mobile challenge
Your assignment involves crafting widget logic inspired by iOS widgets.
The challenge lies in designing dynamic widgets of varying sizes and
retrieving data from a JSON file, with the added bonus of rearranging
them if possible.






## Project stack
This project is crafted with precision and modern standards, ensuring a robust and maintainable codebase:

- Language & Framework: Developed using Swift and SwiftUI, showcasing the latest advancements in iOS development.
- Architecture: Implements the MVVM (Model-View-ViewModel) pattern, promoting a clean separation of concerns and an enhanced development workflow.
- Testing: Enriched with comprehensive Unit Tests and UI Tests, ensuring reliability and facilitating future code changes with confidence.
- Design Pattern: Utilizes the Repository Pattern, abstracting data sources to maintain a seamless and scalable data flow.

## Repository pattern details
This pattern is used here solely for mocking purposes. In `HalaaTaskApp.swift`, you can see that we perform a check to determine if the current session is for UITesting; if so, we inject the testing repository. Otherwise, in the normal sessions of the app, we inject the local repository. Although it is used here only for mocking purposes, it provides a good representation of how we can use this pattern to retrieve data from different sources.
## Authors

- Abdelrahman Badary

